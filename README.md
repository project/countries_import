# Countries

This module imports a list with countries containing information like ISO2 code,
ISO3 code, name, official name and flag. Countries can be stored as terms or
nodes based on the configuration screen.

To configure, go to `/admin/config/content/countries-import`
and choose the fields.
