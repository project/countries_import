<?php

namespace Drupal\countries_import\Form;

use Drupal\content_translation\ContentTranslationManagerInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\countries_import\Services\CountriesService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use TYPO3\PharStreamWrapper\Exception;

/**
 * CountriesSettings form.
 */
class CountriesSettingsForm extends ConfigFormBase {

  const MAPPING_TITLES = [
    'name' => 'Country name',
    'code2l' => 'ISO2',
    'code3l' => 'ISO3',
    'name_official' => 'Country official name',
    'flag' => 'Flag image',
  ];

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  /**
   * The countries service manager.
   *
   * @var \Drupal\countries_import\Services\CountriesService
   */
  protected CountriesService $countriesService;

  /**
   * The translation manager.
   *
   * @var \Drupal\content_translation\ContentTranslationManagerInterface
   */
  protected ContentTranslationManagerInterface $translationManager;

  /**
   * System file configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected Config $config;

  /**
   * Creates a new CountriesSettings form.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   The entity type bundle info.
   * @param \Drupal\countries_import\Services\CountriesService $countriesService
   *   The countries service manager.
   * @param \Drupal\content_translation\ContentTranslationManagerInterface $translationManager
   *   The translation manager.
   */
  public function __construct(ConfigFactoryInterface $configFactory, EntityFieldManagerInterface $entityFieldManager, EntityTypeManagerInterface $entityTypeManager, EntityTypeBundleInfoInterface $entityTypeBundleInfo, CountriesService $countriesService, ContentTranslationManagerInterface $translationManager) {
    parent::__construct($configFactory);
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->countriesService = $countriesService;
    $this->translationManager = $translationManager;
    $this->config = $this->config('countries_import.settings');
  }

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpParamsInspection
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('countries_import.helper'),
      $container->get('content_translation.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'countries_import_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['countries_import.settings'];
  }

  /**
   * {@inheritdoc}
   *
   * @SuppressWarnings(PHPMD.CamelCaseParameterName)
   * @SuppressWarnings(PHPMD.CamelCaseVariableName)
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $entityTypeId = $this->config->get('entity_type_id') ?: 'taxonomy_term';
    if (isset($form_state->getUserInput()['entity_type_id'])) {
      $entityTypeId = $form_state->getUserInput()['entity_type_id'];
    }
    $entityType = $this->entityTypeManager->getDefinition($entityTypeId);
    $bundleOptions = $this->getEntityTypeBundles($entityTypeId);

    $form['#prefix'] = '<div id="countries-import-form-wrapper">';
    $form['#suffix'] = '</div>';
    $form['info'] = [
      0 => [
        '#type' => 'container',
        '#weight' => 0,
        'message' => [
          '#type' => 'markup',
          '#markup' => $this->t('By default, the name of the entity will be "Country name". You can override this configuration by choosing to save another field from below in name.'),
        ],
      ],
    ];
    $form['entity_type_id'] = [
      '#type' => 'radios',
      '#title' => $this->t('Choose storage'),
      '#description' => $this->t("Choose where to store countries"),
      '#default_value' => $entityTypeId,
      '#options' => [
        'taxonomy_term' => $this->t('Taxonomy'),
        'node' => $this->t('Content type'),
      ],
      '#return_value' => TRUE,
      '#required' => TRUE,
      '#weight' => 1,
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => '::updateBundlesList',
        'wrapper' => 'countries-import-form-wrapper',
        'effect' => 'fade',
        'method' => 'replace',
      ],
    ];
    $form['bundle'] = [
      '#type' => 'select',
      '#title' => $entityType->getBundleLabel(),
      '#options' => array_merge(['' => '- None -'], $bundleOptions),
      '#default_value' => $this->config->get('bundle') ?: '',
      '#required' => TRUE,
      '#size' => 1,
      '#multiple' => FALSE,
      '#weight' => 2,
      '#ajax' => [
        'callback' => '::updateEntityListItems',
        'wrapper' => 'countries-import-form-wrapper',
        'effect' => 'fade',
        'method' => 'replace',
      ],
    ];
    $this->setFormFields($form, $form_state, $entityTypeId);
    $form['flag_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Import flags'),
      '#options' => [
        'svg' => $this->t('SVG format'),
        'png-32' => $this->t('PNG format 32x16'),
        'png-128' => $this->t('PNG format 128x64'),
      ],
      '#default_value' => $this->config->get('flag_format') ?: 'svg',
      '#required' => FALSE,
      '#size' => 1,
      '#multiple' => FALSE,
      '#weight' => 10,
      '#states' => [
        'invisible' => [
          ':input[name="flag"]' => ['value' => ''],
        ],
      ],
    ];
    $form['translate'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Translate name and official name'),
      '#default_value' => $form_state->getUserInput()['translate'] ?? $this->config->get('translate'),
      '#weight' => 11,
      '#states' => [
        'invisible' => [
          ':input[name="bundle"]' => ['value' => ''],
        ],
      ],
    ];
    $form['actions']['import'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import Countries'),
      '#weight' => 11,
      '#submit' => [
        [$this, 'submitForm'],
        [$this, 'importCountriesForm'],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Set fields.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state array.
   * @param string $entityTypeId
   *   The entity type ID. Only entity types that implement
   *   \Drupal\Core\Entity\FieldableEntityInterface are supported.
   */
  private function setFormFields(array &$form, FormStateInterface $formState, string $entityTypeId) {
    foreach (self::MAPPING_TITLES as $fieldConfigId => $fieldTitle) {
      $bundle = $formState->getUserInput()['bundle'] ?? $this->config->get('bundle');
      $existingFields = $this->getEntityBundleFields($entityTypeId, $bundle, $fieldConfigId);
      $defaultValue = $formState->getUserInput()[$fieldConfigId] ?? $this->config->get('fields.' . $fieldConfigId) ?: '';
      $defaultValue = in_array($defaultValue, $existingFields) ? $defaultValue : '';

      $form[$fieldConfigId] = [
        '#type' => 'select',
        '#title' => $fieldTitle,
        '#options' => $existingFields,
        '#default_value' => $defaultValue,
        '#required' => (in_array($fieldConfigId, ['code2l', 'code3l'])),
        '#size' => 1,
        '#multiple' => FALSE,
        '#weight' => 5,
        '#validated' => TRUE,
        '#states' => [
          'invisible' => [
            ':input[name="bundle"]' => ['value' => ''],
          ],
        ],
      ];
    }
  }

  /**
   * Creates entities using the configuration.
   *
   * @throws \Exception
   */
  public function importCountriesForm() {
    try {
      $this->countriesService->import();
      $this->messenger()->addStatus($this->t('Countries was imported successfully.'));
    }
    catch (\Exception $e) {
      throw new Exception($e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $formState) {
    $values = $formState->getValues();
    if ($values['translate'] && !$this->translationManager->isEnabled($values['entity_type_id'], $values['bundle'])) {
      $message = $this->t('Translation is not enable for this type of entity.
      Uncheck "Translate name and official name" or enable translation.');
      $formState->setErrorByName('translate', $message);
    }
    parent::validateForm($form, $formState);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function submitForm(array &$form, FormStateInterface $formState) {
    $values = $formState->getValues();

    $this->config->set('bundle', $values['bundle']);
    $this->config->set('entity_type_id', $values['entity_type_id']);
    foreach (array_keys(self::MAPPING_TITLES) as $fieldId) {
      $value = $values[$fieldId] ?? '';
      if (!in_array($value, $form[$fieldId]['#options'])) {
        $value = '';
      }
      $this->config->set('fields.' . $fieldId, $value);
    }
    $this->config->set('flag_format', $values['flag_format']);
    $this->config->set('translate', $values['translate']);
    $this->config->save();

    parent::submitForm($form, $formState);
  }

  /**
   * {@inheritdoc}
   */
  public function updateEntityListItems(array &$form): array {
    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpUnused
   */
  public function updateBundlesList(array &$form): array {
    return $form;
  }

  /**
   * Get an array with fields from a specific bundle.
   *
   * @param string $entityTypeId
   *   The entity type ID. Only entity types that implement.
   * @param string $bundle
   *   The bundle.
   * @param string $fieldConfigId
   *   The field config id.
   *
   * @return array|string[]
   *   Return an array with values.
   */
  private function getEntityBundleFields($entityTypeId, $bundle, $fieldConfigId): array {
    $fields = $this->entityFieldManager->getFieldDefinitions($entityTypeId, $bundle);
    foreach ($fields as $fieldId => $fieldInfo) {
      $type = ($fieldInfo->getType() == 'entity_reference') ? $fieldInfo->getSetting('target_type') : $fieldInfo->getType();
      if (!in_array($type, $this->getAllowedFieldType($fieldConfigId))) {
        unset($fields[$fieldId]);
      }
    }
    return array_merge([NULL => '- None - '], array_combine(array_keys($fields), array_keys($fields)));
  }

  /**
   * Gets the bundle info of an entity type.
   *
   * @param string $entityTypeId
   *   The entity type ID.
   *
   * @return array
   *   An array of bundle information.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getEntityTypeBundles($entityTypeId): array {
    $entityType = $this->entityTypeManager->getDefinition($entityTypeId);
    $bundles = $this->entityTypeBundleInfo->getBundleInfo($entityTypeId);
    $bundleOptions = [];
    if ($entityType->hasKey('bundle')) {
      foreach ($bundles as $bundleName => $bundleInfo) {
        $bundleOptions[$bundleName] = $bundleInfo['label'];
      }
      natsort($bundleOptions);
    }
    return $bundleOptions;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllowedFieldType($field) {
    switch ($field) {
      case 'name':
      case 'code2l':
      case 'code3l':
      case 'name_official':
        return [
          'string',
          'string_long',
          'text',
          'text_long',
          'text_with_summary',
        ];

      case 'flag':
        return [
          'file',
          'image',
          'media',
        ];
    }
  }

}
