<?php

namespace Drupal\countries_import\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\migrate\Exception\EntityValidationException;

/**
 * The CountriesService service.
 */
class CountriesService extends CountriesBaseService {

  /**
   * File utils manager.
   *
   * @var \Drupal\countries_import\Services\FileUtils
   */
  protected FileUtils $fileUtils;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Json with countries.
   *
   * @var string
   */
  protected string $path;

  /**
   * CountriesService constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user interface.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extensionPath
   *   The extension path resolver.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   * @param \Drupal\countries_import\Services\FileUtils $fileUtils
   *   The file utils service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, AccountProxyInterface $currentUser, EntityTypeManagerInterface $entityTypeManager, ExtensionPathResolver $extensionPath, LanguageManagerInterface $languageManager, FileUtils $fileUtils) {
    parent::__construct($configFactory, $currentUser, $entityTypeManager, $extensionPath);
    $this->languageManager = $languageManager;
    $this->fileUtils = $fileUtils;
    $this->path = "https://raw.githubusercontent.com/cristiroma/countries/master/data/countries.json";
  }

  /**
   * Import each country with flag & translation if needed.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\migrate\Exception\EntityValidationException
   */
  public function import() {
    $data = file_get_contents($this->path);
    $items = json_decode($data, TRUE);
    foreach ($items as $item) {
      $properties = [
        $this->getSetting('fields')['code3l'] => $item['code3l'],
      ];
      $entity = $this->entityTypeManager->getStorage($this->getSetting('entity_type_id'))
        ->loadByProperties($properties);
      $entity = reset($entity);
      if (empty($entity)) {
        $entity = $this->createInstance($item['name']);
      }
      $entity = $this->populateFields($entity, $item);
      $errors = $entity->validate();
      if ($errors->count()) {
        throw new EntityValidationException($errors);
      }
      if ($this->getSetting('translate')) {
        $this->translateEntity($entity, $item);
      }
      $entity->save();
    }
  }

  /**
   * Populate entity's fields.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object being populated.
   * @param array $item
   *   Array with information.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   */
  private function populateFields(EntityInterface $entity, array $item): EntityInterface {
    foreach ($this->getSetting('fields') as $key => $field) {
      if (!$field) {
        continue;
      }
      if ($key == 'flag') {
        $basename = $this->buildBasename($item['code2l']);
        $file = $this->fileUtils->prepareFileEntity($basename);
        $entity->set($field, ['target_id' => $file->id()]);
        continue;
      }
      $entity->set($field, $item[$key]);
    }
    return $entity;
  }

  /**
   * Add translations for entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object being translated.
   * @param array $item
   *   Array with information.
   */
  private function translateEntity(EntityInterface $entity, array $item) {
    $languages = $this->languageManager->getLanguages();
    unset($languages[$this->languageManager->getDefaultLanguage()->getId()]);

    foreach ($languages as $language) {
      $langcode = $language->getId();
      if (!in_array($langcode, array_keys($item['names']))) {
        continue;
      }
      $translateEntity = [];
      $hasTranslation = $entity->hasTranslation($langcode);
      foreach ($item['names'][$langcode] as $field => $value) {
        $fieldMachineName = $this->getSetting('fields')[$field];
        if (!$fieldMachineName) {
          continue;
        }
        if ($hasTranslation) {
          $entity->getTranslation($langcode)->$fieldMachineName = $value;
          continue;
        }
        $translateEntity[$fieldMachineName] = $value;
      }
      if (!$hasTranslation) {
        $entity->addTranslation($langcode, $translateEntity);
      }
    }
  }

  /**
   * Create a new entity.
   *
   * @param string $name
   *   The name of the entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The new entity.
   */
  private function createInstance(string $name): EntityInterface {
    if ($this->getSetting('entity_type_id') == 'taxonomy_term') {
      return $this->entityTypeManager->getStorage('taxonomy_term')->create([
        'vid' => $this->getSetting('bundle'),
        'name' => $name,
      ]);
    }
    $entity = $this->entityTypeManager->getStorage('node')->create([
      'type' => $this->getSetting('bundle'),
      'title' => $name,
    ]);
    $entity->setOwnerId($this->currentUser->id());
    return $entity;
  }

  /**
   * Build basename.
   *
   * @param string $code2l
   *   The ISO 2 code (e.g.: RO)
   *
   * @return string
   *   Return basename.
   */
  private function buildBasename(string $code2l): string {
    switch ($this->getSetting('flag_format')) {
      case 'png-32':
        $basename = "$code2l-32.png";
        break;

      case 'png-128':
        $basename = "$code2l-128.png";
        break;

      case 'svg':
        $basename = "$code2l.svg";
        break;

      default:
        $basename = "$code2l.svg";
    }
    return $basename;
  }

}
