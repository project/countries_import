<?php

namespace Drupal\countries_import\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Field\FieldException;
use Drupal\Core\File\Exception\DirectoryNotReadyException;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\file\FileInterface;

/**
 * Provides a service to handle file for country.
 */
class FileUtils extends CountriesBaseService {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * Constructor for FileUtils.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user interface.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extensionPath
   *   The extension path resolver.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system manager.
   */
  public function __construct(ConfigFactoryInterface $configFactory, AccountProxyInterface $currentUser, EntityTypeManagerInterface $entityTypeManager, ExtensionPathResolver $extensionPath, EntityFieldManagerInterface $entityFieldManager, FileSystemInterface $fileSystem) {
    $this->entityFieldManager = $entityFieldManager;
    $this->fileSystem = $fileSystem;
    parent::__construct($configFactory, $currentUser, $entityTypeManager, $extensionPath);
  }

  /**
   * Prepare file.
   *
   * This function finds the flag's path based on the basename. Prepares the
   * directory, tries to copy the file into the new location, and saves as a
   * media if needed.
   *
   * @param string $basename
   *   The basename (e.g: RO.svg).
   *
   * @return \Drupal\file\Entity\File|\Drupal\file\FileInterface|\Drupal\media\Entity\Media|false
   *   Return a file or a media entity if the file was successfully created,
   *   FALSE otherwise.
   */
  public function prepareFileEntity(string $basename) {
    $source = $this->extensionPath->getPath('module', 'countries_import')
      . sprintf('/images/flags/%s/%s', $this->getSetting('flag_format'), $basename);

    $fieldConfig = $this->getFieldConfig();

    $dir = $this->prepareDirectory($fieldConfig);
    $destination = $dir . '/' . $basename;
    $file = $this->copyFile($source, $destination);
    if ($fieldConfig->getTargetEntityTypeId() != 'media') {
      return $file;
    }
    $entityFieldName = $fieldConfig->get('field_name');
    $media = $this->mediaStorage->loadByProperties([
      'thumbnail.target_id' => $file->id(),
    ]);
    $media = reset($media);
    if (empty($media)) {
      $media = $this->mediaStorage->create([
        'bundle' => $fieldConfig->getTargetBundle(),
        'name' => $file->getFilename(),
        $entityFieldName => [
          [
            'target_id' => $file->id(),
            'alt' => $file->getFilename(),
            'title' => $file->getFilename(),
          ],
        ],
      ]);
      $media->save();
    }

    return $media;
  }

  /**
   * {@inheritDoc}
   */
  private function getFieldConfig() {
    $flag = $this->getSetting('fields')['flag'];
    $fieldDefinitions = $this->entityFieldManager->getFieldDefinitions($this->getSetting('entity_type_id'), $this->getSetting('bundle'));
    $fieldConfig = $fieldDefinitions[$flag];
    $handlerSettings = $fieldConfig->getSetting('handler_settings');
    $targetType = $fieldConfig->getSetting('target_type');
    if ($fieldConfig->getType() == 'entity_reference') {
      if (empty($handlerSettings['target_bundles'])) {
        throw new FieldException('Target bundles for field %field could not be found.', ['%field' => $flag]);
      }
      $allowedBundles = $handlerSettings['target_bundles'];
      $bundle = reset($allowedBundles);

      $fieldDefinitions = $this->entityFieldManager->getFieldDefinitions($targetType, $bundle);
      foreach ($fieldDefinitions as $fieldId => $field) {
        if (!$field instanceof FieldConfig) {
          unset($fieldDefinitions[$fieldId]);
        }
      }
      $fieldConfig = reset($fieldDefinitions);
    }
    return $fieldConfig;
  }

  /**
   * Checks that the directory exists and is writable.
   *
   * @param \Drupal\field\Entity\FieldConfig $fieldConfig
   *   The field definition.
   *
   * @return string
   *   The directory if exists and is writable.
   */
  private function prepareDirectory(FieldConfig $fieldConfig): string {
    $dir = $fieldConfig->getSetting('uri_scheme') . '://' . $fieldConfig->getSetting('file_directory');
    $result = $this->fileSystem->prepareDirectory($dir, FileSystemInterface::CREATE_DIRECTORY);
    if (!$result) {
      throw new DirectoryNotReadyException('The directory %directory does not exist or is not writable.', ['%directory' => $dir]);
    }

    return $dir;
  }

  /**
   * Copies a file to a new location and return a file or exception.
   *
   * @param string $source
   *   Path to the file source.
   * @param string $destination
   *   Path to the source file.
   *
   * @return \Drupal\file\FileInterface|false
   *   File entity if the copy is successful, or FALSE in the event of an error.
   */
  private function copyFile(string $source, string $destination): FileInterface {
    try {
      if (!file_exists($destination) && !$this->fileSystem->copy($source, $destination, FileSystemInterface::EXISTS_REPLACE)) {
        throw new FileException("File '$source' could not be copied");
      }
    }
    catch (FileException $e) {
      return FALSE;
    }
    // If we are replacing an existing file, load it.
    $file = $this->loadByUri($destination);
    if (!$file) {
      $filename = pathinfo($destination, PATHINFO_FILENAME);
      $file = $this->fileStorage->create([
        'uri' => $destination,
        'status' => 1,
        'filename' => $filename,
        'filemime' => mime_content_type($destination),
        'uid' => $this->currentUser->id(),
      ]);
      $file->save();
    }
    return $file;
  }

  /**
   * Loads the first File entity found with the specified URI.
   *
   * @param string $uri
   *   The file URI.
   *
   * @return \Drupal\file\FileInterface|null
   *   The first file with the matched URI if found, NULL otherwise.
   *
   * @see \Drupal\file\FileRepositoryInterface::loadByUri()
   */
  private function loadByUri(string $uri): ?FileInterface {
    /** @var \Drupal\file\FileInterface[] $files */
    $files = $this->fileStorage->loadByProperties(['uri' => $uri]);
    if (count($files)) {
      foreach ($files as $item) {
        // Since some database servers sometimes use a case-insensitive
        // comparison by default, double check that the filename is an exact
        // match.
        if ($item->getFileUri() === $uri) {
          return $item;
        }
      }
    }
    return NULL;
  }

}
